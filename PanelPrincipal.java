import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class PanelPrincipal extends JPanel {

    private JMenuBar barraMenu;
    private JMenu archivo;
    private JMenuItem nuevoArchivo;
    private JMenuItem abrirArchivo;
    private JMenuItem guardarArchivo;

    private JMenu vista;
    private JMenuItem maximizar;
    private JMenuItem minimizar;
    private JMenuItem estandar;
    private JMenu ayuda;
    private JMenuItem informacion;
    private JMenuItem acercaDe;
    private PanelTexto panelTexto;
    private Color colorLila;

    public PanelPrincipal(Ventana ventana) {

        panelTexto = new PanelTexto();
        
        barraMenu = new JMenuBar();
        archivo = new JMenu("Archivo");
        nuevoArchivo = new JMenuItem("Nuevo");
        abrirArchivo = new JMenuItem("Abrir");
        guardarArchivo = new JMenuItem("Guardar");

        vista = new JMenu("Vista");
        maximizar = new JMenuItem("Maximizar");
        minimizar = new JMenuItem("Minimizar");
        estandar = new JMenuItem("Estándar");

        maximizar.addActionListener(e -> ventana.setExtendedState(JFrame.MAXIMIZED_BOTH));
        minimizar.addActionListener(e -> ventana.setExtendedState(JFrame.ICONIFIED));
        estandar.addActionListener(e -> ventana.setExtendedState(JFrame.NORMAL));

        ayuda = new JMenu("Ayuda");
        informacion = new JMenuItem("Ayuda al usuario");
        acercaDe = new JMenuItem("Acerca de textSimulator");

        colorLila = new Color(177, 158, 218);
        personalizarMenu(colorLila);

        barraMenu.setBorder(null);
        barraMenu.setBackground(colorLila);

        archivo.add(nuevoArchivo);
        archivo.add(abrirArchivo);
        archivo.add(guardarArchivo);
        barraMenu.add(archivo);

        abrirArchivo.addActionListener(new AccionAbrir());
        guardarArchivo.addActionListener(new AccionGuardar());
        nuevoArchivo.addActionListener(new Nuevo());

        vista.add(maximizar);
        vista.add(minimizar);
        vista.add(estandar);
        barraMenu.add(vista);

        ayuda.add(informacion);
        ayuda.add(acercaDe);
        barraMenu.add(ayuda);
        informacion.addActionListener(new Help());
        acercaDe.addActionListener(new Help());
        setLayout(new BorderLayout());
        add(barraMenu, BorderLayout.NORTH);
        add(panelTexto, BorderLayout.CENTER);

        setVisible(true);

    }

    private void personalizarMenu(Color color) {
        nuevoArchivo.setBackground(color);
        abrirArchivo.setBackground(color);
        guardarArchivo.setBackground(color);
        minimizar.setBackground(color);
        maximizar.setBackground(color);
        estandar.setBackground(color);
        informacion.setBackground(color);
        acercaDe.setBackground(color);
    }

    private class AccionAbrir implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {

            if (panelTexto.getTextPane().getText() != "") {
                int opcion = JOptionPane.showConfirmDialog(null,
                        "Al abrir un nuevo archivo perderá los cambios no guardados\nDesea continuar?");
                if (opcion == JOptionPane.NO_OPTION || opcion == JOptionPane.CANCEL_OPTION) {
                    return;
                }
            }
            JFileChooser selector = new JFileChooser();
            selector.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            int seleccion = selector.showOpenDialog(null);

            if (seleccion == JFileChooser.APPROVE_OPTION) {
                try {
                    File file = selector.getSelectedFile();
                    FileReader reader = new FileReader(file);
                    BufferedReader bReader = new BufferedReader(reader);
                    String linea = bReader.readLine();
                    Document doc = panelTexto.getTextPane().getDocument();
                    panelTexto.getTextPane().setText("");
                    while (linea != null) {
                        linea += "\n";
                        doc.insertString(doc.getLength(), linea, null);
                        linea = bReader.readLine();
                    }
                    bReader.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BadLocationException e) {
                    e.printStackTrace();
                }

            }    
        }
    }

    public class AccionGuardar implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            JFileChooser chooser = new JFileChooser();
            chooser.showSaveDialog(null);

            if (chooser.getSelectedFile().exists()) {
                int opc = JOptionPane.showConfirmDialog(null, "Desea sobreescribir el archivo?", "ALERTA",
                        JOptionPane.YES_NO_OPTION);
                if (!(opc == JOptionPane.YES_OPTION)) {
                    return;
                }
            }

            File file = chooser.getSelectedFile();
            try {
                FileWriter fWriter = new FileWriter(file, false);
                BufferedWriter bfWriter = new BufferedWriter(fWriter);
                bfWriter.write(panelTexto.getTextPane().getText());
                bfWriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public class Nuevo implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Ventana nuevaVentana = new Ventana();
            nuevaVentana.setVisible(true);
        }
    }

    public class Help implements ActionListener {
    
         @Override
         public void actionPerformed(ActionEvent e) {
            
            String aux = "";
            String textInfo = "";
            String[] texto;
            String part1 = "";
            String part2 = "";

            try {   
                BufferedReader leer = new BufferedReader(new FileReader("README.txt"));
                while ((aux = leer.readLine()) != null) {
                textInfo += aux + "\n";
            }
            texto = textInfo.split("\\@");
            part1 = texto[0];
            part2 = texto[1];
            leer.close();         
            } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error Importando - " + e);
            }
          
            if (e.getSource() == informacion){  
            JOptionPane.showMessageDialog(null,part1);
            } else if (e.getSource() == acercaDe){
            JOptionPane.showMessageDialog(null,part2);
            }
            
        }

    }

}