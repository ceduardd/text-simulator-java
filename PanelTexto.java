import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.*;
import java.awt.Color;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class PanelTexto extends JPanel {

    private JPanel panelSuperior;
    private JScrollPane scrollPane;
    private JTextPane textPane;
    private JComboBox<String> fuentes;
    private JComboBox<Integer> size;
    private JToggleButton negrita;
    private JToggleButton cursiva;
    private JButton color;
    private Color colorAzul;
    private Color colorLila;
    private Color colorPanel;

    public PanelTexto() {
        panelSuperior = new JPanel();
        textPane = new JTextPane();
        scrollPane = new JScrollPane(textPane);
        negrita = new JToggleButton("N");
        cursiva = new JToggleButton("K");
        color = new JButton("Color");
       
        
        colorAzul = new Color(158, 169, 218);
        colorLila = new Color(177, 158, 218);
        colorPanel = new Color( 192, 194, 200 );
        textPane.setBackground(colorPanel);
        scrollPane.getVerticalScrollBar().setBackground(colorAzul);
        scrollPane.getHorizontalScrollBar().setBackground(colorAzul);
               
        llenarComboFuente();
        llenarComboSize();

        fuentes.addActionListener(new AccionEstilos());
        size.addActionListener(new AccionEstilos());
        negrita.addActionListener(new AccionEstilos());
        cursiva.addActionListener(new AccionEstilos());
	    color.addActionListener( e -> {
            Color color = JColorChooser.showDialog(this, "Seleccione el color", Color.WHITE);
            textPane.setForeground(color);
        });

        personalizarComponente(fuentes);
        personalizarComponente(size);
        personalizarComponente(negrita);
        personalizarComponente(cursiva);
        personalizarComponente(color);
        
        cursiva.setFont(new Font(cursiva.getFont().getFamily(), Font.ITALIC|Font.BOLD,  cursiva.getFont().getSize()));
        
        panelSuperior.add(fuentes);
        panelSuperior.add(size);
        panelSuperior.add(negrita);
        panelSuperior.add(cursiva);
        panelSuperior.add(color);
        
        panelSuperior.setBackground(colorLila);
        
        setLayout(new BorderLayout());
        add(panelSuperior, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
    }

    private int getEstilo() {
        
        if(negrita.isSelected() && cursiva.isSelected()) {
            return Font.BOLD|Font.ITALIC;
        } else if(negrita.isSelected()) {
            return Font.BOLD;
        } else if(cursiva.isSelected()) {
            return Font.ITALIC;
        } else {
            return Font.PLAIN;
        }

    }

    private class AccionEstilos implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            textPane.setFont(new Font(fuentes.getSelectedItem().toString(), getEstilo(), (Integer)size.getSelectedItem()));

        }

    } 

    private void llenarComboFuente() {
        String[] listaFuentes = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        fuentes = new JComboBox<>(listaFuentes);
        fuentes.setSelectedItem("Arial");
    } 

    private void llenarComboSize() {
        Integer[] listaSize = {8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40,
                                  42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 64, 68, 72, 76, 80  };
        size = new JComboBox<>(listaSize);
        size.setSelectedItem(14);
    } 

    private void personalizarComponente(JComponent componente) {
        componente.setBackground(colorAzul);
    }

    public JTextPane getTextPane() {
        return this.textPane;
    } 
  
}
