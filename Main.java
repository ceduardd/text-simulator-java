/**
 * Developers:
 * -Eduardo Chávez
 * Samuel Hereira
 * Jonathan Tapia
 * Joel Villao
 * Bryan Lindao
 */


public class Main {

    public static void main(String[] args) {
        Ventana ventana = new Ventana();
        ventana.setVisible(true);
    }

}