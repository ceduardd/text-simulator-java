                                          ------------------- INFORMACION -------------------
Archivo
Nuevo: En la pestaña Nuevo se crea un nuevo archivo de texto para empezar desde cero.
Abrir: En la pestaña Abrir se permite acceder a un archivo de texto previamente guardado en un dispositivo o una carpeta.
Guardar: En la pestaña Guardar se permite conservar la información realizada en el editor de texto.

Vista
Maximizar: Esta pestaña permite ampliar al area de trabajo a pantalla completa.
Minimizar: Esta pestaña permite quitar la ventana del escritorio, mientras el programa sigue funcionando.
Estandar: Esta pestaña permite mantener el tamaño estandar recomendado.

Fuentes: Cambia los estilo del texto.
Tamano: Cambia el tamaño del texto.
Negrita: La función de letras negritas es destacar parte del texto.
Cursiva: La función de letras cursivas es señalar una parte del texto que se distingue del resto.
Color: El botón color permite establecer el color de letra de un texto.

@                       ------------------- ACERCA DE TEXTSIMULATOR -------------------

TEXTSIMULATION
Version 1.0(compilacion de SO 17763.1036)
2020 TEXTSIMULATOR. Todos los derechos reservados.

El editor de texto TEXTSIMULATOR y su interfaz de usuario estan protegidos
por las leyes de marca comercial y otros derechos de propiedad intelectual actuales
y pendientes en el Ecuador y otros paises o regiones

Colaboradores:

-Eduardo Chavez
-Samuel Hereira
-Bryan Lindao
-Jonathan Tapia
-Joel Villao