import javax.swing.JFrame;

public class Ventana extends JFrame {

    public Ventana() {
        
        setTitle("textSimulator v.1");
        setSize(600,600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        add(new PanelPrincipal(this));

    }

}